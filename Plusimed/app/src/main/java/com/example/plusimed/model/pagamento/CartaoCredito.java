package com.example.plusimed.model.pagamento;

import com.example.plusimed.model.pagamento.Customer;
import com.example.plusimed.model.pagamento.Payment;
import com.example.plusimed.model.pagamento.Transaction;

import java.util.ArrayList;

public class CartaoCredito {
    private String token_account;
    Customer CustomerObject;
    ArrayList < Object > transaction_product = new ArrayList < Object > ();
    Transaction TransactionObject;
    Payment PaymentObject;


    // Getter Methods


    public ArrayList<Object> getTransaction_product() {
        return transaction_product;
    }


    public void setTransaction_product(ArrayList<Object> transaction_product) {
        this.transaction_product = transaction_product;
    }

    public String getToken_account() {
        return token_account;
    }

    public Customer getCustomer() {
        return CustomerObject;
    }

    public Transaction getTransaction() {
        return TransactionObject;
    }

    public Payment getPayment() {
        return PaymentObject;
    }

    // Setter Methods

    public void setToken_account(String token_account) {
        this.token_account = token_account;
    }

    public void setCustomer(Customer customerObject) {
        this.CustomerObject = customerObject;
    }

    public void setTransaction(Transaction transactionObject) {
        this.TransactionObject = transactionObject;

    }

    public void setCustomerObject(Customer customerObject) {
        CustomerObject = customerObject;
    }

    public void setTransactionObject(Transaction transactionObject) {
        TransactionObject = transactionObject;
    }

    public void setPaymentObject(Payment paymentObject) {
        PaymentObject = paymentObject;
    }

    public void setPayment(Payment paymentObject) {
        this.PaymentObject = paymentObject;
    }


}


