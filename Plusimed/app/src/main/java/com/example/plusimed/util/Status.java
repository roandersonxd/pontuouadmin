package com.example.plusimed.util;

public class Status {



    public static String getStatus(int status_code){
        switch (status_code){
            case 4:{
                return "Aguardando Pagamento";
            }
            case 5:{
                return "Em Processamento";
            }
            case 6:{
                return "Aprovada";
            }
            case 7:{
                return "Cancelada";
            }
            case 24:{
                return "Em Contestação";
            }
            case 87:{
                return "Em Monitoramento";
            }
            case 89:{
                return "Reprovada";
            }
        }
        return null;
    }




}
