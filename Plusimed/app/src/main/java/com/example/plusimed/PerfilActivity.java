package com.example.plusimed;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.plusimed.adapter.CustomSpinnerFormaPagamento;
import com.example.plusimed.adapter.CustomSpinnerPlano;
import com.example.plusimed.model.Cliente;
import com.example.plusimed.model.Plano;
import com.example.plusimed.model.pagamento.Formapagamento;
import com.example.plusimed.service.ClienteService;
import com.example.plusimed.service.FormapagamentoService;
import com.example.plusimed.service.PlanoService;
import com.example.plusimed.util.Paths;
import com.example.plusimed.util.Session;
import com.example.plusimed.util.Status;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PerfilActivity extends AppCompatActivity {

    TextView tv_name;
    TextView tv_email;
    TextView tv_telefone;
    TextView tv_cidade;
    TextView tv_estado;
    TextView tv_endereco;
    TextView tv_bairro;
    TextView tv_empresa;
    Button btn_salvar;
    Button bt_cod_barras;
    Spinner spinner_pagamento;
    Spinner sppiner_plano;
    Button btn_pagamento;
    TextView tv_status;
    private Session session;
    private FormapagamentoService formapagamentoService;
    private PlanoService planoService;
    private ClienteService clienteService;
    private ProgressDialog progressDialog;
    private Context context = this;
    private ArrayList<Formapagamento> formapagamentosList =  new ArrayList<>();
    private ArrayList<Plano> planosList =  new ArrayList<>();
    private Cliente cliente;

    private Formapagamento formapagamentoSelecionado;
    private Plano planoSelecionado;




    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Perfil");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go back
                onBackPressed();

            }
        });


        session = new Session(this);
        loadElements();

        Intent i = getIntent();
        setCliente((Cliente) i.getSerializableExtra("Cliente"));
        Log.d("Cliente",cliente.toString());

        btn_salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog(context,"","",true);
               if(cliente!=null){
                   if(cliente.getId_plano()!=planoSelecionado.getId()||cliente.getId_formapagamento()!=formapagamentoSelecionado.getId()){
                       updateCliente((int) cliente.getId(),planoSelecionado.getId(),formapagamentoSelecionado.getId());
                   }
               }

            }
        });

        loadData(cliente);
        configRetrofit();
        getFormapagamentos();
        getPlanos();

        showProgressDialog(context, "", "", true);
    }


    public void loadElements(){
        tv_name = (TextView)findViewById(R.id.profile_name);
        tv_email = (TextView)findViewById(R.id.profile_email);
        tv_telefone = (TextView)findViewById(R.id.profile_phone);
        tv_cidade  = (TextView)findViewById(R.id.profile_city);
        tv_estado  = (TextView)findViewById(R.id.profile_state);
        tv_endereco = (TextView)findViewById(R.id.profile_address);
        tv_bairro  = (TextView)findViewById(R.id.profile_neighborhood);
        tv_empresa  = (TextView)findViewById(R.id.profile_enterprise);
        spinner_pagamento = (Spinner)findViewById(R.id.spinner_pagamento);
        sppiner_plano = (Spinner)findViewById(R.id.spinner_plano);
        btn_salvar  = (Button)findViewById(R.id.btn_salvar);
        btn_pagamento = (Button)findViewById(R.id.btn_pagamento);
        bt_cod_barras = (Button)findViewById(R.id.bt_cod_barras);
        tv_status = (TextView)findViewById(R.id.tv_status);


    }
    public void loadData(final Cliente cliente){
        tv_name.setText(cliente.getNmCliente());
        tv_email.setText(cliente.getEmail());
        tv_telefone.setText(cliente.getNrfone());
        tv_cidade.setText(cliente.getNmcidade());
        tv_estado.setText(cliente.getCduf());
        tv_endereco.setText(cliente.getNmEndereco());
        tv_bairro.setText(cliente.getNmbairro());
        tv_empresa.setText(cliente.getEmpresa());

        if(cliente.getSituacao().equals("Ativo")){
            btn_pagamento.setVisibility(View.GONE);
            bt_cod_barras.setVisibility(View.GONE);
            tv_status.setVisibility(View.GONE);
        }else{
            btn_pagamento.setVisibility(View.VISIBLE);
            bt_cod_barras.setVisibility(View.GONE);
            tv_status.setVisibility(View.GONE);
            btn_pagamento.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(formapagamentoSelecionado.getDsforma().equals("Boleto")){
                        showProgressDialog(context, "", "", true);
                        gerarBoleto((int)cliente.getId());
                    }else{
                        Intent i = new Intent(PerfilActivity.this, CreditCardFragmentActivity.class);
                        cliente.setId_plano(planoSelecionado.getId());
                        cliente.setId_formapagamento(planoSelecionado.getId());
                        Intent intent = new Intent(PerfilActivity.this, CreditCardFragmentActivity.class);
                        Bundle b = new Bundle();
                        b.putInt("id_cliente", (int) cliente.getId());
                        b.putDouble("vlPlano",planoSelecionado.getVlplano());
                        intent.putExtras(b);
                        startActivity(intent);
                        finish();
                    }

                }
            });
            bt_cod_barras.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setClipboard(PerfilActivity.this,tv_status.getText().toString());
                }
            });
        }

    }

    public void configRetrofit(){


        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);




        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Paths.URL_BASE_API)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit  = builder.build();
        formapagamentoService = retrofit.create(FormapagamentoService.class);
        planoService = retrofit.create(PlanoService.class);

        clienteService = retrofit.create(ClienteService.class);

    }
    private void getFormapagamentos(){


        Call<List<Formapagamento>> call  = formapagamentoService.getFormasPagamentos("Bearer "+ Session.getToken());

        call.enqueue(new Callback<List<Formapagamento>>() {
            @Override
            public void onResponse(Call<List<Formapagamento>> call, Response<List<Formapagamento>> response) {
                List<Formapagamento>formapagamentos = null;
                if(response.isSuccessful()){
                    formapagamentos = response.body();
                    setAdapterCustomSpinner((ArrayList<Formapagamento>) formapagamentos);
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(PerfilActivity.this,"OPS! Não conseguimos carregar seus dados!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Formapagamento>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(PerfilActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void getPlanos(){


        Call<List<Plano>> call  = planoService.getPlanos("Bearer "+ Session.getToken());

        call.enqueue(new Callback<List<Plano>>() {
            @Override
            public void onResponse(Call<List<Plano>> call, Response<List<Plano>> response) {
                List<Plano>planos = null;
                if(response.isSuccessful()){
                    planos = response.body();
                    setAdapterCustomSpinnePlanos((ArrayList<Plano>) planos);
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(PerfilActivity.this,"OPS! Não conseguimos carregar seus dados!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Plano>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(PerfilActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void updateCliente(int id, int id_plano, int id_formapagamento){

        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("id_plano",id_plano);
        jsonParams.put("id_formapagamento",id_formapagamento);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(new JSONObject(jsonParams)).toString());


        Call<ResponseBody> call  = clienteService.updateCliente("Bearer "+ Session.getToken(),id,body);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.isSuccessful()){

                    try {
                        Log.d("cliente",response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(PerfilActivity.this,"OPS! Não conseguimos carregar seus dados!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(PerfilActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void gerarBoleto(int id_cliente){
        Call<ResponseBody> call  = clienteService.gerarBoleto("Bearer "+ Session.getToken(),id_cliente);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String result = null;
                int status;
                int codigo_barras;
                if(response.isSuccessful()){

                    try {

                        result= response.body().string();
                        Log.d("boleto",result);
                        JSONObject jsonObj = new JSONObject(result);
                        status = jsonObj.getInt("status_code");
                        codigo_barras = jsonObj.getInt("codigo_barras");
                        tv_status.setVisibility(View.VISIBLE);
                        bt_cod_barras.setVisibility(View.VISIBLE);
                        tv_status.setText(Status.getStatus(status));
                        progressDialog.dismiss();


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(PerfilActivity.this,"OPS! Não conseguimos carregar seus dados!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(PerfilActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void showProgressDialog(Context context, String tile, String msg, boolean b){
        progressDialog = ProgressDialog.show(context,"Carregando...", "Por Favor, aguarde enquanto carregamos seus dados.");
        progressDialog.setCancelable(false);

    }

    public void setAdapterCustomSpinner(ArrayList<Formapagamento>formapagamentos){
        CustomSpinnerFormaPagamento adapter = new CustomSpinnerFormaPagamento(context,formapagamentos);
        spinner_pagamento.setAdapter(adapter);
        for(int i =0;i<formapagamentos.size();i++){
            if(formapagamentos.get(i).getId()==getCliente().getId_formapagamento()){
                spinner_pagamento.setSelection(i);
            }
        }

        spinner_pagamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                 formapagamentoSelecionado = (Formapagamento)adapterView.getSelectedItem();
                // Toast.makeText(PerfilActivity.this,formapagamentoSelecionado.getDsforma(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void setAdapterCustomSpinnePlanos(ArrayList<Plano>planos){
        CustomSpinnerPlano adapter = new CustomSpinnerPlano(context,planos);
        sppiner_plano.setAdapter(adapter);
        for(int i =0;i<planos.size();i++){
            if(planos.get(i).getId()==getCliente().getId_plano()){
                sppiner_plano.setSelection(i);
            }
        }

        sppiner_plano.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                planoSelecionado = (Plano) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void setClipboard(Context context, String text) {

            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Código de barras: ", text);
            clipboard.setPrimaryClip(clip);

    }



}
