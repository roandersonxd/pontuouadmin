package com.example.plusimed.service;

import com.example.plusimed.model.Especialidade;
import com.example.plusimed.model.Medico;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface MedicoService {


    @GET("medico/{id_especialidade}")
    Call<List<Medico>> getMedicos(@Header("Authorization") String token, @Path("id_especialidade")int id_especialidade);
}
