package com.example.plusimed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.R;
import com.example.plusimed.model.Especialidade;
import com.example.plusimed.model.pagamento.Formapagamento;

import java.util.ArrayList;


public class CustomSpinnerEspecialidade extends ArrayAdapter<Especialidade>{

    public CustomSpinnerEspecialidade(@NonNull Context context, ArrayList<Especialidade>especialidades) {
        super(context, 0,especialidades);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_especialiade,parent,false);
        }
        Especialidade especialidade = getItem(position);
        ImageView ivespecialidade = convertView.findViewById(R.id.ivespecialidade);
        TextView tvespecialidade  = convertView.findViewById(R.id.tv_especialidade);
        ivespecialidade.setImageResource(R.drawable.stethoscope);
        tvespecialidade.setText(especialidade.getNmEspecialidade());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_especialiade,parent,false);
        }
        Especialidade especialidade = getItem(position);
        ImageView ivespecialidade = convertView.findViewById(R.id.ivespecialidade);
        TextView tvespecialidade  = convertView.findViewById(R.id.tv_especialidade);
        tvespecialidade.setText(especialidade.getNmEspecialidade());
        ivespecialidade.setImageResource(R.drawable.stethoscope);
        return convertView;
    }
}
