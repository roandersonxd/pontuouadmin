package com.example.plusimed.model;

public class Especialidade {


    private int id;
    private String nmEspecialidade;

    public Especialidade(){

    }

    public Especialidade(int id, String nmEspecialidade) {
        this.id = id;
        this.nmEspecialidade = nmEspecialidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNmEspecialidade() {
        return nmEspecialidade;
    }

    public void setNmEspecialidade(String nmEspecialidade) {
        this.nmEspecialidade = nmEspecialidade;
    }



}
