package com.example.plusimed.model;

public class Clinica {

    private int clinica_id;
    private String latitude;
    private String longitude;
    private String nmclinica;

    public Clinica() {
    }

    public Clinica(int clinica_id, String latitude, String longitude, String nmclinica) {
        this.clinica_id = clinica_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.nmclinica = nmclinica;
    }

    public int getClinica_id() {
        return clinica_id;
    }

    public void setClinica_id(int clinica_id) {
        this.clinica_id = clinica_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNmclinica() {
        return nmclinica;
    }

    public void setNmclinica(String nmclinica) {
        this.nmclinica = nmclinica;
    }

    @Override
    public String toString() {
        return "Clinica{" +
                "clinica_id=" + clinica_id +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", nmclinica='" + nmclinica + '\'' +
                '}';
    }
}
