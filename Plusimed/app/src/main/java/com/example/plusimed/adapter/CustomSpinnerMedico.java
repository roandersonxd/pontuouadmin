package com.example.plusimed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.R;
import com.example.plusimed.model.Especialidade;
import com.example.plusimed.model.Medico;

import java.util.ArrayList;


public class CustomSpinnerMedico extends ArrayAdapter<Medico>{

    public CustomSpinnerMedico(@NonNull Context context, ArrayList<Medico>medicos) {
        super(context, 0,medicos);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_medico,parent,false);
        }
        Medico medico = getItem(position);
        ImageView ivmedico = convertView.findViewById(R.id.ivmedico);
        TextView tvespecialidade  = convertView.findViewById(R.id.tv_medico);
        ivmedico.setImageResource(R.drawable.doctor);
        tvespecialidade.setText(medico.getNmmedico());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_medico,parent,false);
        }
        Medico medico = getItem(position);
        ImageView ivmedico = convertView.findViewById(R.id.ivmedico);
        TextView tvespecialidade  = convertView.findViewById(R.id.tv_medico);
        ivmedico.setImageResource(R.drawable.doctor);
        tvespecialidade.setText(medico.getNmmedico());
        return convertView;
    }
}
