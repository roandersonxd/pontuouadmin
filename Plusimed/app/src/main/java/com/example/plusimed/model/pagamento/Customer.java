package com.example.plusimed.model.pagamento;

import java.util.ArrayList;


public class Customer {
    ArrayList< Object > contacts = new ArrayList < Object > ();
    ArrayList < Object > addresses = new ArrayList < Object > ();
    private String name;
    private String birth_date;
    private String cpf;
    private String email;


    // Getter Methods

    public String getName() {
        return name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    // Setter Methods

    public void setName(String name) {
        this.name = name;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Object> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Object> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Object> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Object> addresses) {
        this.addresses = addresses;
    }
}