package com.example.plusimed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.R;
import com.example.plusimed.model.pagamento.Formapagamento;

import java.util.ArrayList;


public class CustomSpinnerFormaPagamento  extends ArrayAdapter<Formapagamento>{

    public CustomSpinnerFormaPagamento(@NonNull Context context, ArrayList<Formapagamento>formapagamentos) {
        super(context, 0,formapagamentos);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_forma_pagamento,parent,false);
        }
        Formapagamento formapagamento = getItem(position);
        ImageView ivformapagamento = convertView.findViewById(R.id.ivformapagamento);
        TextView tvformapagamento  = convertView.findViewById(R.id.tv_formapagamento);
        if(formapagamento.getDsforma().equals("Boleto")){
            ivformapagamento.setImageResource(R.drawable.ic_money_black_24dp);
        }
        if(formapagamento.getDsforma().equals("Cartão")){
            ivformapagamento.setImageResource(R.drawable.ic_credit_card_black_24dp);
        }
        tvformapagamento.setText(formapagamento.getDsforma());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_forma_pagamento,parent,false);
        }
        Formapagamento formapagamento = getItem(position);
        ImageView ivformapagamento = convertView.findViewById(R.id.ivformapagamento);
        TextView tvformapagamento  = convertView.findViewById(R.id.tv_formapagamento);
        if(formapagamento.getDsforma().equals("Boleto")){
            ivformapagamento.setImageResource(R.drawable.ic_money_black_24dp);
        }
        if(formapagamento.getDsforma().equals("Cartão")){
            ivformapagamento.setImageResource(R.drawable.ic_credit_card_black_24dp);
        }
        tvformapagamento.setText(formapagamento.getDsforma());
        return convertView;
    }
}
