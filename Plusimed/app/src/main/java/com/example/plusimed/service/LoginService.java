package com.example.plusimed.service;



import com.example.plusimed.model.Login;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface LoginService {

    @POST("token")
    Call<ResponseBody> login(@Body Login login);
}
