package com.example.plusimed.model.pagamento;

public class Transaction {
    private String available_payment_methods;
    private String customer_ip;
    private String shipping_type;
    private String shipping_price;
    private String price_discount;
    private String url_notification;
    private String free;


    // Getter Methods

    public String getAvailable_payment_methods() {
        return available_payment_methods;
    }

    public String getCustomer_ip() {
        return customer_ip;
    }

    public String getShipping_type() {
        return shipping_type;
    }

    public String getShipping_price() {
        return shipping_price;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public String getUrl_notification() {
        return url_notification;
    }

    public String getFree() {
        return free;
    }

    // Setter Methods

    public void setAvailable_payment_methods(String available_payment_methods) {
        this.available_payment_methods = available_payment_methods;
    }

    public void setCustomer_ip(String customer_ip) {
        this.customer_ip = customer_ip;
    }

    public void setShipping_type(String shipping_type) {
        this.shipping_type = shipping_type;
    }

    public void setShipping_price(String shipping_price) {
        this.shipping_price = shipping_price;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public void setUrl_notification(String url_notification) {
        this.url_notification = url_notification;
    }

    public void setFree(String free) {
        this.free = free;
    }
}