package com.example.plusimed.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {
    private static SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    public Session(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public boolean setLogin(boolean status) {
        spEditor = sp.edit();
        spEditor.putBoolean("is_logged_in", status);
        spEditor.commit();
        return true;
    }


    public void setToken(String token){
        spEditor = sp.edit();
        spEditor.putString("token", token);
        spEditor.commit();
    }

    public void setID(int id){
        spEditor = sp.edit();
        spEditor.putInt("idUser", id);
        spEditor.commit();
    }

    public static String getToken(){
        return sp.getString("token", "");
    }
    public static int getID(){
        return sp.getInt("idUser", 0);
    }
    public boolean getLoggedIn() {
        return sp.getBoolean("is_logged_in", false);
    }
}
