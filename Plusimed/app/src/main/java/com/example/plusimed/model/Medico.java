package com.example.plusimed.model;

public class Medico {

    private int medico_id;
    private String nmmedico;


    public Medico() {
    }


    public Medico(int medico_id, String nmmedico) {
        this.medico_id = medico_id;
        this.nmmedico = nmmedico;
    }

    public int getMedico_id() {
        return medico_id;
    }

    public void setMedico_id(int medico_id) {
        this.medico_id = medico_id;
    }

    public String getNmmedico() {
        return nmmedico;
    }

    public void setNmmedico(String nmmedico) {
        this.nmmedico = nmmedico;
    }

    @Override
    public String toString() {
        return "Medico{" +
                "medico_id=" + medico_id +
                ", nmmedico='" + nmmedico + '\'' +
                '}';
    }
}
