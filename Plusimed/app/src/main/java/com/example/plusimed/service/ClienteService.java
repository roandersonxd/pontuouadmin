package com.example.plusimed.service;

import com.example.plusimed.model.Cliente;
import com.example.plusimed.model.User;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ClienteService {


    @GET("user")
    Call<User> getUser(@Header("Authorization")String token);

    @GET("cliente/{email_user}")
    Call<Cliente> getCliente(@Header("Authorization")String token, @Path("email_user") String emailUser);

    @Headers({"Content-Type: application/json","Accept: application/json"})
    @PUT("cliente/{id}")
    Call<ResponseBody> updateCliente(@Header("Authorization")String token, @Path("id") int id, @Body RequestBody jsonObject);

    @POST("cliente/{id_cliente}")
    Call<ResponseBody> gerarBoleto(@Header("Authorization")String token, @Path("id_cliente") int id);


    @Headers({"Content-Type: application/json","Accept: application/json"})
    @PUT("status/cliente/{id}")
    Call<ResponseBody> setStatus(@Header("Authorization")String token, @Path("id") int id, @Body RequestBody body);


    /*@GET("cliente/{email_user}")
    Call<ResponseBody> getCliente(@Header("Authorization")String token,@Path("email_user") String emailUser);*/


}
