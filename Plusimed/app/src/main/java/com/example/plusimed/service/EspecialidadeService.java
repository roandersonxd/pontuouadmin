package com.example.plusimed.service;

import com.example.plusimed.model.Especialidade;
import com.example.plusimed.model.Plano;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface EspecialidadeService {


    @GET("especialidade")
    Call<List<Especialidade>> getEspecialidades(@Header("Authorization")String token);
}
