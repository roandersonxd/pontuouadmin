package com.example.plusimed;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.plusimed.model.Cliente;
import com.example.plusimed.service.ClienteService;
import com.example.plusimed.service.FormapagamentoService;
import com.example.plusimed.util.Paths;
import com.example.plusimed.util.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import movile.com.creditcardguide.ActionOnPayListener;
import movile.com.creditcardguide.CreditCardFragment;
import movile.com.creditcardguide.model.CreditCardPaymentMethod;
import movile.com.creditcardguide.model.IssuerCode;
import movile.com.creditcardguide.model.PaymentMethod;
import movile.com.creditcardguide.model.PurchaseOption;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;




public class CreditCardFragmentActivity extends AppCompatActivity implements ActionOnPayListener {

    private CreditCardFragment inputCardFragment;
    private Session session;
    private FormapagamentoService formapagamentoService;
    private ClienteService clienteService;
    private ProgressDialog progressDialog;
    private Context context = this;


    Dialog succesDialog;
    ImageView close;
    Button btn_ok;
    TextView tv_titulo_msg;
    TextView tv_descricao_msg;
    private int id_cliente = 0;
    private double vlPlano;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_fragment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("PlusiMed");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new Session(this);

        Bundle b = getIntent().getExtras();
         id_cliente = 0;

        if(b != null){
            setId_cliente(b.getInt("id_cliente"));
            setVlPlano(b.getDouble("vlPlano"));
        }


        configRetrofit();

        succesDialog  = new Dialog(this);


        inputCardFragment = (CreditCardFragment) getFragmentManager().findFragmentById(R.id.frg_pay);

        inputCardFragment.setPagesOrder(CreditCardFragment.Step.FLAG, CreditCardFragment.Step.NUMBER,
                CreditCardFragment.Step.EXPIRE_DATE, CreditCardFragment.Step.CVV, CreditCardFragment.Step.NAME);

        inputCardFragment.setListPurchaseOptions(getList(), getVlPlano());



    }

    private List<PurchaseOption> getList() {
        List<PurchaseOption> list = new ArrayList<>();
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.MASTERCARD, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.VISACREDITO, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.AMEX, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.PAYPAL, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.DINERS, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.NUBANK, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.AURA, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.ELO, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.HIPERCARD, 6));
        list.add(new PurchaseOption(PaymentMethod.Type.CREDIT_CARD, IssuerCode.OTHER, 6));

        return list;
    }

    @Override
    public void onChangedPage(CreditCardFragment.Step page) {

    }

    @Override
    public void onComplete(CreditCardPaymentMethod purchaseOption, boolean saveCard) {
      //  Toast.makeText(this, purchaseOption.toString(), Toast.LENGTH_LONG).show();
        efetuarPagamento();
        showProgressDialog(context, "", "", true);
        if (saveCard) {
            purchaseOption.setSecurityCode(null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        inputCardFragment.backPressed();
    }


    public void efetuarPagamento(){


        String json_str = "{  \n" +
                "              \"token_account\":\"7973a30d7aecb68\",\n" +
                "              \"customer\":{  \n" +
                "                \"contacts\":[  \n" +
                "                  {  \n" +
                "                    \"type_contact\":\"H\",\n" +
                "                    \"number_contact\":\"1133221122\"\n" +
                "                  },\n" +
                "                  {  \n" +
                "                    \"type_contact\":\"M\",\n" +
                "                    \"number_contact\":\"11999999999\"\n" +
                "                  }\n" +
                "                ],\n" +
                "                \"addresses\":[  \n" +
                "                  {  \n" +
                "                    \"type_address\":\"B\",\n" +
                "                    \"postal_code\":\"17000-000\",\n" +
                "                    \"street\":\"Av Esmeralda\",\n" +
                "                    \"number\":\"1001\",\n" +
                "                    \"completion\":\"A\",\n" +
                "                    \"neighborhood\":\"Jd Esmeralda\",\n" +
                "                    \"city\":\"Marilia\",\n" +
                "                    \"state\":\"SP\"\n" +
                "                  }\n" +
                "                ],\n" +
                "                \"name\":\"Stephen Strange\",\n" +
                "                \"birth_date\":\"21/05/1941\",\n" +
                "                \"cpf\":\"50235335142\",\n" +
                "                \"email\":\"stephen.strange@avengers.com\"\n" +
                "              },\n" +
                "              \"transaction_product\":[  \n" +
                "                {  \n" +
                "                  \"description\":\"Camiseta Tony Stark\",\n" +
                "                  \"quantity\":\"1\",\n" +
                "                  \"price_unit\":\"130.00\",\n" +
                "                  \"code\":\"1\",\n" +
                "                  \"sku_code\":\"0001\",\n" +
                "                  \"extra\":\"Informação Extra\"\n" +
                "                }\n" +
                "              ],\n" +
                "              \"transaction\":{  \n" +
                "                \"available_payment_methods\":\"2,3,4,5,6,7,14,15,16,18,19,21,22,23\",\n" +
                "                \"customer_ip\":\"127.0.0.1\",\n" +
                "                \"shipping_type\":\"Sedex\",\n" +
                "                \"shipping_price\":\"12\",\n" +
                "                \"price_discount\":\"\",\n" +
                "                \"url_notification\":\"\",\n" +
                "                \"free\":\"Campo Livre\"\n" +
                "              },\n" +
                "              \"payment\":{  \n" +
                "                \"payment_method_id\":\"3\",\n" +
                "                \"card_name\":\"STEPHEN STRANGE\",\n" +
                "                \"card_number\":\"4111111111111111\",\n" +
                "                \"card_expdate_month\":\"01\",\n" +
                "                \"card_expdate_year\":\"2021\",\n" +
                "                \"card_cvv\":\"644\",\n" +
                "                \"split\":\"1\"\n" +
                "              }\n" +
                "            }";

        JSONObject my_obj = null;
         String oi = json_str.replaceFirst("(?s)^\\((.*)\\)$", "$1");
        try {
            my_obj = new JSONObject(oi);
            Log.d("LOG CARTAO ",my_obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }





        Call<ResponseBody> call  = formapagamentoService.pagamentoCartaoCredito(RequestBody.create(MediaType.parse("application/json"), my_obj.toString()));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = null;
                if(response.isSuccessful()){
                    showSucessMsg("Sucesso!","Pagamento aprovado com sucesso!");
                    updateStatusCliente(getId_cliente(),"Ativo");
                    progressDialog.dismiss();

                    try {
                        Log.d("oi",response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else{
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(CreditCardFragmentActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void configRetrofit(){


        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);




        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Paths.URL_PAGAMENTO)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit  = builder.build();


        Retrofit.Builder builder_default = new Retrofit.Builder()
                .baseUrl(Paths.URL_BASE_API)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit_default  = builder_default.build();
        formapagamentoService = retrofit.create(FormapagamentoService.class);
        clienteService = retrofit_default.create(ClienteService.class);

    }

    public void showSucessMsg(String titulo,String msg){
        succesDialog.setContentView(R.layout.msg_success_layout);
        close = (ImageView)succesDialog.findViewById(R.id.iv_close);
        btn_ok = (Button)succesDialog.findViewById(R.id.btn_ok_msg);
        tv_titulo_msg = (TextView)succesDialog.findViewById(R.id.tv_titulo_msg);
        tv_descricao_msg = (TextView)succesDialog.findViewById(R.id.tv_descricao_msg);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                succesDialog.dismiss();
                Intent i = new Intent(CreditCardFragmentActivity.this, MenuActivity.class);
                startActivity(i);
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                succesDialog.dismiss();
                Intent i = new Intent(CreditCardFragmentActivity.this, MenuActivity.class);
                startActivity(i);
            }
        });


        tv_titulo_msg.setText(titulo);
        tv_descricao_msg.setText(msg);



        succesDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        succesDialog.show();


    }

    private void showProgressDialog(Context context, String tile, String msg, boolean b){
        progressDialog = ProgressDialog.show(context,"Efetuando pagamento...", "Por Favor, aguarde enquanto é feito o pagamento.");
        progressDialog.setCancelable(false);
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public double getVlPlano() {
        return vlPlano;
    }

    public void setVlPlano(double vlPlano) {
        this.vlPlano = vlPlano;
    }

    private void updateStatusCliente(int id, String status){

        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("situacao",status);


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(new JSONObject(jsonParams)).toString());


        Call<ResponseBody> call  = clienteService.setStatus("Bearer "+ Session.getToken(),id,body);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.isSuccessful()){

                    try {
                        Log.d("cliente",response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(CreditCardFragmentActivity.this,"OPS!Não conseguimos atualizar seus dados!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(CreditCardFragmentActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
