package com.example.plusimed.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.plusimed.R;

public class PlanoViewHolder extends RecyclerView.ViewHolder{

    private TextView tvNome;
    private ImageView ivImagem;
    private ProgressBar progressBar;

    public PlanoViewHolder(View itemView) {
        super(itemView);

        ivImagem = (ImageView) itemView.findViewById(R.id.iv_imagem_categoria);
        tvNome = (TextView) itemView.findViewById(R.id.tvCategoria);
        progressBar = (ProgressBar) itemView.findViewById(R.id.pb_categoria);

    }

    public TextView getTvNome() {
        return tvNome;
    }

    public ImageView getIvImagem() {
        return ivImagem;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }


}