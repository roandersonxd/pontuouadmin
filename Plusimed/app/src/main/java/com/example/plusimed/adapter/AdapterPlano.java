package com.example.plusimed.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.plusimed.R;
import com.example.plusimed.model.Plano;
import com.example.plusimed.util.LoadImage;

import java.util.List;

public class AdapterPlano extends RecyclerView.Adapter<PlanoViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<Plano> planoList;


    public AdapterPlano(Context context, List<Plano> planoList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.planoList = planoList;

    }

    @Override
    public PlanoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_planos, parent, false);
        PlanoViewHolder viewHolder = new PlanoViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PlanoViewHolder holder, int position) {
        holder.getProgressBar().setVisibility(View.VISIBLE);
        holder.getTvNome().setText(planoList.get(position).getNmplano());

        LoadImage.loadImage(context,"https://www.unibanco.pt/wp-content/uploads/2018/02/Seguro-Plano-Fam%C3%ADlia.jpg",holder.getProgressBar(),holder.getIvImagem());



    }

    @Override
    public int getItemCount() {
        return planoList.size();
    }
}

