package com.example.plusimed.model.pagamento;

public class Payment {
    private String payment_method_id;
    private String card_name;
    private String card_number;
    private String card_expdate_month;
    private String card_expdate_year;
    private String card_cvv;
    private String split;


    // Getter Methods

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public String getCard_name() {
        return card_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public String getCard_expdate_month() {
        return card_expdate_month;
    }

    public String getCard_expdate_year() {
        return card_expdate_year;
    }

    public String getCard_cvv() {
        return card_cvv;
    }

    public String getSplit() {
        return split;
    }

    // Setter Methods

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public void setCard_expdate_month(String card_expdate_month) {
        this.card_expdate_month = card_expdate_month;
    }

    public void setCard_expdate_year(String card_expdate_year) {
        this.card_expdate_year = card_expdate_year;
    }

    public void setCard_cvv(String card_cvv) {
        this.card_cvv = card_cvv;
    }

    public void setSplit(String split) {
        this.split = split;
    }
}