package com.example.plusimed.service;

import com.example.plusimed.model.Plano;
import com.example.plusimed.model.pagamento.Formapagamento;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface PlanoService {

    @GET("plano")
    Call<List<Plano>> getPlanos(@Header("Authorization")String token);
}
