package com.example.plusimed.model;

public class Hora {

    private int id;
    private String hora;
    private int data_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getData_id() {
        return data_id;
    }

    public void setData_id(int data_id) {
        this.data_id = data_id;
    }

    public Hora() {
    }

    public Hora(int id, String hora, int data_id) {
        this.id = id;
        this.hora = hora;
        this.data_id = data_id;
    }

    @Override
    public String toString() {
        return "Hora{" +
                "id=" + id +
                ", hora='" + hora + '\'' +
                ", data_id=" + data_id +
                '}';
    }
}

