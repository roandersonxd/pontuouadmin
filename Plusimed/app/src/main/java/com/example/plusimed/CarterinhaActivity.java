package com.example.plusimed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.model.Cliente;
import com.example.plusimed.util.Paths;
import com.google.android.gms.maps.SupportMapFragment;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

public class CarterinhaActivity extends AppCompatActivity {

    private Cliente cliente;
    private TextView tv_nome;
    private TextView tv_matricula;
    private TextView tv_cpf;
    private TextView tv_validade;
    private ImageView ivfoto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carterinha);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Carterinha Virtual");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go back
                onBackPressed();

            }
        });

        Intent i = getIntent();
        setCliente((Cliente) i.getSerializableExtra("Cliente"));
        Log.d("Cliente",cliente.toString());
        loadElements();
        loadData(cliente);


    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    private void loadElements(){
        tv_nome = (TextView)findViewById(R.id.tv_nome);
        tv_cpf  = (TextView)findViewById(R.id.tv_cpf);
        tv_matricula = (TextView)findViewById(R.id.tv_matricula);
        tv_validade = (TextView)findViewById(R.id.tv_validade);

        ivfoto = (ImageView)findViewById(R.id.iv_foto);


    }

    public void loadData(Cliente cliente){
        tv_nome.setText(cliente.getNmCliente());
        tv_cpf.setText(cliente.getNrcpf());
        tv_matricula.setText(String.valueOf(cliente.getId()));
        Picasso.get().load(Paths.URL_IMAGE+cliente.getFoto()).error(R.drawable.avatar)
                .into(ivfoto);
    }
}
