package com.example.plusimed;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.plusimed.adapter.AdapterPlano;
import com.example.plusimed.model.Plano;
import com.example.plusimed.service.ClienteService;
import com.example.plusimed.service.FormapagamentoService;
import com.example.plusimed.service.PlanoService;
import com.example.plusimed.util.Paths;
import com.example.plusimed.util.Session;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlanosActivity extends AppCompatActivity {

    private PlanoService planoService;
    private RecyclerView recyclerView;
    private Context context = this;
    private AdapterPlano adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Planos");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go back
                onBackPressed();

            }
        });

        loadElements();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        configRetrofit();
        getPlanos();

    }

    public void configRetrofit(){


        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);




        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Paths.URL_BASE_API)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit  = builder.build();
        planoService = retrofit.create(PlanoService.class);
    }

    public void loadElements(){
        recyclerView= (RecyclerView) findViewById(R.id.recycler);
    }

    private void getPlanos(){


        Call<List<Plano>> call  = planoService.getPlanos("Bearer "+ Session.getToken());

        call.enqueue(new Callback<List<Plano>>() {
            @Override
            public void onResponse(Call<List<Plano>> call, Response<List<Plano>> response) {
                List<Plano>planos = null;
                if(response.isSuccessful()){
                    planos = response.body();
                    adapter = new AdapterPlano(context,planos);
                    recyclerView.setAdapter(adapter);

                }else{
                    Toast.makeText(PlanosActivity.this,"OPS! Não conseguimos carregar os planos!",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<List<Plano>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(PlanosActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();

            }
        });
    }
}
