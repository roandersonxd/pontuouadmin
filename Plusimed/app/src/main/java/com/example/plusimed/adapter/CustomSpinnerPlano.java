package com.example.plusimed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.R;
import com.example.plusimed.model.Plano;
import com.example.plusimed.model.pagamento.Formapagamento;

import java.util.ArrayList;


public class CustomSpinnerPlano extends ArrayAdapter<Plano>{

    public CustomSpinnerPlano(@NonNull Context context, ArrayList<Plano>planos) {
        super(context, 0,planos);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_plano,parent,false);
        }
        Plano plano = getItem(position);
        TextView tvplano  = convertView.findViewById(R.id.tv_plano);
        tvplano.setText(plano.getNmplano() + "- R$ "+plano.getVlplano());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_plano,parent,false);
        }
        Plano plano = getItem(position);

        TextView tvplano  = convertView.findViewById(R.id.tv_plano);
        tvplano.setText(plano.getNmplano() + "- R$ "+plano.getVlplano());
        return convertView;

    }
}
