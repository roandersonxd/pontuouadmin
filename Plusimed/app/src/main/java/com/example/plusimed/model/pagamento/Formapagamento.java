package com.example.plusimed.model.pagamento;

public class Formapagamento {

    private int id;
    private String dsforma;
    private int nrdias;

    public Formapagamento() {
    }

    public Formapagamento(int id, String dsforma, int nrdias) {
        this.id = id;
        this.dsforma = dsforma;
        this.nrdias = nrdias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDsforma() {
        return dsforma;
    }

    public void setDsforma(String dsforma) {
        this.dsforma = dsforma;
    }

    public int getNrdias() {
        return nrdias;
    }

    public void setNrdias(int nrdias) {
        this.nrdias = nrdias;
    }

    @Override
    public String toString() {
        return "Formapagamento{" +
                "id=" + id +
                ", dsforma='" + dsforma + '\'' +
                ", nrdias=" + nrdias +
                '}';
    }
}
