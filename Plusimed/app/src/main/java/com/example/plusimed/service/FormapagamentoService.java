package com.example.plusimed.service;

import com.example.plusimed.model.pagamento.CartaoCredito;
import com.example.plusimed.model.pagamento.Formapagamento;



import org.json.JSONObject;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;

public interface FormapagamentoService {


    @GET("formapagamento")
    Call<List<Formapagamento>> getFormasPagamentos(@Header("Authorization")String token);


    @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("payment")
    Call<ResponseBody> pagamentoCartaoCredito(@Body RequestBody jsonObject);


}
