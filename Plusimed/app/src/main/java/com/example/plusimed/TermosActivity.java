package com.example.plusimed;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class TermosActivity  extends AppCompatActivity {
    Context context;

    WebView infoWebview;
    SwipeRefreshLayout swipeLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_termos);
        infoWebview = (WebView) findViewById(R.id.webview_info);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);

        context = this;

        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Termos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go back
                onBackPressed();

            }
        });


        // Swipe to Refresh
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // refreshes the WebView
                refresh();
            }
        });

        //stop refresh
        infoWebview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                swipeLayout.setRefreshing(false);
            }
        });
        //enable javascript
        infoWebview.getSettings().setJavaScriptEnabled(true);
        // infoWebview.clearCache(false);
        //infoWebview.getSettings().setMinimumFontSize((int) getResources().getDimension(R.dimen.webviewMinTextSize));
        //infoWebview.getSettings().setBuiltInZoomControls(true);
        //infoWebview.getSettings().setDisplayZoomControls(false);

        //load info
        refresh();
    }


    public void refresh() {
        infoWebview.loadUrl("");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        infoWebview.destroy();
    }

    //added after v41
    @Override
    public void onPause() {
        super.onPause();
        infoWebview.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        infoWebview.onResume();
    }
}