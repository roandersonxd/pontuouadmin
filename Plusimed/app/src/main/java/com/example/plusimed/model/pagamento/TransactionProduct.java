package com.example.plusimed.model.pagamento;

public class TransactionProduct {

    private String description;
    private String quantity;
    private String price_unit;
    private String code;
    private String sku_code;
    private String extra;


    public TransactionProduct(String description, String quantity, String price_unit, String code, String sku_code, String extra) {
        this.description = description;
        this.quantity = quantity;
        this.price_unit = price_unit;
        this.code = code;
        this.sku_code = sku_code;
        this.extra = extra;
    }

    public TransactionProduct() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice_unit() {
        return price_unit;
    }

    public void setPrice_unit(String price_unit) {
        this.price_unit = price_unit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSku_code() {
        return sku_code;
    }

    public void setSku_code(String sku_code) {
        this.sku_code = sku_code;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
