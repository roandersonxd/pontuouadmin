package com.example.plusimed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.R;
import com.example.plusimed.model.Data;
import com.example.plusimed.model.Hora;

import java.util.ArrayList;


public class CustomSpinnerHora extends ArrayAdapter<Hora>{

    public CustomSpinnerHora(@NonNull Context context, ArrayList<Hora>horas) {
        super(context, 0,horas);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_hora,parent,false);
        }
        Hora hora = getItem(position);
        ImageView ivhora = convertView.findViewById(R.id.ivhora);
        TextView tvhora  = convertView.findViewById(R.id.tv_hora);
        ivhora.setImageResource(R.drawable.ic_time_black_24dp);
        tvhora.setText(hora.getHora());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_hora,parent,false);
        }
        Hora hora = getItem(position);
        ImageView ivhora = convertView.findViewById(R.id.ivhora);
        TextView tvhora  = convertView.findViewById(R.id.tv_hora);
        ivhora.setImageResource(R.drawable.ic_time_black_24dp);
        tvhora.setText(hora.getHora());
        return convertView;
    }
}
