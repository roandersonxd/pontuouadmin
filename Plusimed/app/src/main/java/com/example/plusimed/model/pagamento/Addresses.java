package com.example.plusimed.model.pagamento;

public class Addresses {

    private String type_address;
    private String postal_code;
    private String street;
    private String number;
    private String completion;
    private String neighborhood;
    private String city;
    private String state;

    public Addresses() {
    }

    public Addresses(String type_address, String postal_code, String street, String number, String completion, String neighborhood, String city, String state) {
        this.type_address = type_address;
        this.postal_code = postal_code;
        this.street = street;
        this.number = number;
        this.completion = completion;
        this.neighborhood = neighborhood;
        this.city = city;
        this.state = state;
    }

    public String getType_address() {
        return type_address;
    }

    public void setType_address(String type_address) {
        this.type_address = type_address;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCompletion() {
        return completion;
    }

    public void setCompletion(String completion) {
        this.completion = completion;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
