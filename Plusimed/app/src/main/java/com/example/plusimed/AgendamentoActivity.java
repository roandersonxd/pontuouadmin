package com.example.plusimed;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.plusimed.adapter.CustomSpinnerData;
import com.example.plusimed.adapter.CustomSpinnerEspecialidade;
import com.example.plusimed.adapter.CustomSpinnerFormaPagamento;
import com.example.plusimed.adapter.CustomSpinnerHora;
import com.example.plusimed.adapter.CustomSpinnerMedico;
import com.example.plusimed.model.Cliente;
import com.example.plusimed.model.Clinica;
import com.example.plusimed.model.Data;
import com.example.plusimed.model.Especialidade;
import com.example.plusimed.model.Hora;
import com.example.plusimed.model.Medico;
import com.example.plusimed.model.Plano;
import com.example.plusimed.model.pagamento.Formapagamento;
import com.example.plusimed.service.AgendamentoService;
import com.example.plusimed.service.ClienteService;
import com.example.plusimed.service.ClinicaService;
import com.example.plusimed.service.EspecialidadeService;
import com.example.plusimed.service.FormapagamentoService;
import com.example.plusimed.service.MedicoService;
import com.example.plusimed.service.PlanoService;
import com.example.plusimed.util.DatePickerFragment;
import com.example.plusimed.util.Paths;
import com.example.plusimed.util.Session;
import com.example.plusimed.util.TimerPickerFragment;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgendamentoActivity extends AppCompatActivity implements OnMapReadyCallback
        , LocationListener, GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener{

    Dialog succesDialog;
    ImageView close;
    Button btn_ok;
    TextView tv_titulo_msg;
    TextView tv_descricao_msg;

    Spinner spinner_especialidade;
    Spinner spinner_medico;
    Spinner spinner_data;
    Spinner spinner_hora;

    Button btn_solicitar_agendamento;

    private Session session;

    private EspecialidadeService especialidadeService;
    private MedicoService medicoService;
    private ClinicaService clinicaService;
    private AgendamentoService agendamentoService;


    private ProgressDialog progressDialog;
    private Context context = this;
    private ArrayList<Especialidade> especialidades =  new ArrayList<>();
    private Especialidade especialidadeSelecionado;

    private ArrayList<Medico> medicos =  new ArrayList<>();
    private Medico medicoSelecionado;


    private ArrayList<Data> datas =  new ArrayList<>();
    private Data dataSelecionado;


    private ArrayList<Hora> horas =  new ArrayList<>();
    private Hora horaSelecionado;

    private ArrayList<Clinica> clinicas =  new ArrayList<>();

    private AlertDialog dialog;



    private int id_clinica;

    private static final int MY_PERMISSION_CODE = 1000;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private double latitude,longitude;
    private Location mLastLocation;
    private Marker mMarker;
    private LocationRequest mLocationRequest;
    private Cliente cliente;


    private Clinica clinicaSelecionada;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamento);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Agendamento");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go back
                onBackPressed();

            }
        });
        succesDialog  = new Dialog(this);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            checkLocationPermission();
        }

        Intent i = getIntent();
        setCliente((Cliente) i.getSerializableExtra("Cliente"));


        loadElements();
        configRetrofit();
        getEspecialidades();
        showProgressDialog(this,"Carregando...","Carregando especialidades",true);

       btn_solicitar_agendamento.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               showProgressDialog(AgendamentoActivity.this,"Agendamento...","Solicitando agendamento.",true);
               solicitarAgendamento(medicoSelecionado.getMedico_id(), (int) cliente.getId(), especialidadeSelecionado.getId(), dataSelecionado.getId(),horaSelecionado.getId(),clinicaSelecionada.getClinica_id());
           }
       });
    }


    private void getEspecialidades(){

        Call<List<Especialidade>> call  = especialidadeService.getEspecialidades("Bearer "+ Session.getToken());

        call.enqueue(new Callback<List<Especialidade>>() {
            @Override
            public void onResponse(Call<List<Especialidade>> call, Response<List<Especialidade>> response) {
                List<Especialidade>especialidades = null;
                if(response.isSuccessful()){
                    especialidades = response.body();
                    Log.d("Especialidades",especialidades.toString());
                    setAdapterCustomSpinner((ArrayList<Especialidade>) especialidades);
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(AgendamentoActivity.this,"OPS! Não conseguimos carregar as especialidades!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Especialidade>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AgendamentoActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void getMedicos(int id_especialidade){

        Call<List<Medico>> call  = medicoService.getMedicos("Bearer "+ Session.getToken(),id_especialidade);

        call.enqueue(new Callback<List<Medico>>() {
            @Override
            public void onResponse(Call<List<Medico>> call, Response<List<Medico>> response) {
                List<Medico>medicos = null;
                if(response.isSuccessful()){
                    medicos = response.body();
                    Log.d("Medicos",medicos.toString());
                    setAdapterCustomSpinnerMedicos((ArrayList<Medico>) medicos);
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(AgendamentoActivity.this,"OPS! Não conseguimos carregar os médicos!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Medico>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AgendamentoActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void getClinicas(int id_clinica){

        Call<List<Clinica>> call  = clinicaService.getClinicas("Bearer "+ Session.getToken(),id_clinica);

        call.enqueue(new Callback<List<Clinica>>() {
            @Override
            public void onResponse(Call<List<Clinica>> call, Response<List<Clinica>> response) {

                if(response.isSuccessful()){
                    clinicas = (ArrayList<Clinica>) response.body();
                    Log.d("Clinicas",clinicas.toString());
                    for(int i =0;i<clinicas.size();i++){
                        MarkerOptions  markerOptions = new MarkerOptions();
                        double latitude = Double.parseDouble(clinicas.get(i).getLatitude());
                        double longitude = Double.parseDouble(clinicas.get(i).getLongitude());
                        LatLng latLng  = new LatLng(latitude,longitude);
                        markerOptions.title(clinicas.get(i).getNmclinica());
                        markerOptions.position(latLng);
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));


                        markerOptions.snippet(String.valueOf(i));
                        mMap.addMarker(markerOptions);

                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

                    }

                    progressDialog.dismiss();

                }else{
                    Toast.makeText(AgendamentoActivity.this,"OPS! Não conseguimos carregar os médicos!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Clinica>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AgendamentoActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    public void setAdapterCustomSpinnerData(final ArrayList<Data>datas){
        CustomSpinnerData adapter = new CustomSpinnerData(context,datas);
        spinner_data.setAdapter(adapter);


        spinner_data.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dataSelecionado = (Data) adapterView.getSelectedItem();
                showProgressDialog(AgendamentoActivity.this,"Carregando...","Carregando horas disponíveis",true);
                getHoras(dataSelecionado.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setAdapterCustomSpinner(final ArrayList<Especialidade>especialidades){
        CustomSpinnerEspecialidade adapter = new CustomSpinnerEspecialidade(context,especialidades);
        spinner_especialidade.setAdapter(adapter);


        spinner_especialidade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                especialidadeSelecionado = (Especialidade) adapterView.getSelectedItem();
                //Toast.makeText(AgendamentoActivity.this,String.valueOf(especialidadeSelecionado.getId()),Toast.LENGTH_SHORT).show();
                showProgressDialog(AgendamentoActivity.this,"Carregando...","Carregando médicos",true);
                getMedicos(especialidadeSelecionado.getId());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void setAdapterCustomSpinnerMedicos(final ArrayList<Medico>medicos){
        CustomSpinnerMedico adapter = new CustomSpinnerMedico(context,medicos);
        spinner_medico.setAdapter(adapter);


        spinner_medico.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                medicoSelecionado = (Medico) adapterView.getSelectedItem();
                showProgressDialog(AgendamentoActivity.this,"Carregando...","Carregando clínicas",true);
                getClinicas(medicoSelecionado.getMedico_id());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setAdapterCustomSpinnerHora(final ArrayList<Hora>horas){
        CustomSpinnerHora adapter = new CustomSpinnerHora(context,horas);
        spinner_hora.setAdapter(adapter);


        spinner_hora.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                horaSelecionado = (Hora) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void configRetrofit(){


        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);




        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Paths.URL_BASE_API)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit  = builder.build();
        especialidadeService = retrofit.create(EspecialidadeService.class);
        medicoService = retrofit.create(MedicoService.class);
        clinicaService = retrofit.create(ClinicaService.class);
        agendamentoService = retrofit.create(AgendamentoService.class);


    }

    public void loadElements(){
        spinner_especialidade = (Spinner)findViewById(R.id.spinner_especialidade);
        spinner_medico = (Spinner)findViewById(R.id.spinner_medico);
        spinner_data = (Spinner) findViewById(R.id.spinner_data);
        spinner_hora = (Spinner) findViewById(R.id.spinner_hora);
        btn_solicitar_agendamento = (Button)findViewById(R.id.btn_solicitar_agendamento);





    }

    private void showProgressDialog(Context context, String tile, String msg, boolean b){
        progressDialog = ProgressDialog.show(context,tile, msg);
        progressDialog.setCancelable(b);

    }



    private boolean checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, MY_PERMISSION_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, MY_PERMISSION_CODE);
            }

            return true;
        }else{
            return  false;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSION_CODE:{
                if(grantResults.length > 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                        if(mGoogleApiClient ==null){
                            buildGoogleApiClient();
                            mMap.setMyLocationEnabled(true);
                        }
                    }
                }else{
                    Toast.makeText(this,"Permissão negada.",Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);

            }
        }else{
            googleMap.setMyLocationEnabled(true);
            googleMap.setMyLocationEnabled(true);
        }
        //
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if(marker.getSnippet().equals("")|| marker.getSnippet()==null){

                }else{
                    id_clinica = Integer.parseInt(marker.getSnippet());
                    int idClinica = clinicas.get(id_clinica).getClinica_id();
                    clinicaSelecionada  = clinicas.get(id_clinica);

                    if( medicoSelecionado.getMedico_id()==0){

                    }else{
                        showDialog(clinicas.get(id_clinica).getNmclinica(),"Deseja selecionar essa clínica?",idClinica);

                    }
                }




                return true;
            }
        });
    }


    private synchronized  void  buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation  = location;
        if(mMarker!=null)
            mMarker.remove();


        latitude = location.getLatitude();
        longitude = location.getLongitude();

        LatLng latLng   = new LatLng(latitude,longitude);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                .title("Sua posição")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mMarker = mMap.addMarker(markerOptions);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        if(mGoogleApiClient!=null)
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);
        }

    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void getDatas(int id_medico,int id_clinica){

        Call<List<Data>> call  = agendamentoService.getDatas("Bearer "+ Session.getToken(),id_medico,id_clinica);

        call.enqueue(new Callback<List<Data>>() {
            @Override
            public void onResponse(Call<List<Data>> call, Response<List<Data>> response) {
                if(response.isSuccessful()){
                    datas = (ArrayList<Data>) response.body();
                    Log.d("Datas",datas.toString());
                    setAdapterCustomSpinnerData((ArrayList<Data>) datas);
                    spinner_data.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(AgendamentoActivity.this,"OPS! Não conseguimos carregar as datas!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Data>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AgendamentoActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    public void getHoras(int id_data){

        Toast.makeText(context,String.valueOf(id_data),Toast.LENGTH_SHORT).show();

        Call<List<Hora>> call  = agendamentoService.getHoras("Bearer "+ Session.getToken(),id_data);

        call.enqueue(new Callback<List<Hora>>() {
            @Override
            public void onResponse(Call<List<Hora>> call, Response<List<Hora>> response) {
                if(response.isSuccessful()){
                    horas = (ArrayList<Hora>) response.body();
                    Log.d("hora",horas.toString());
                    setAdapterCustomSpinnerHora((ArrayList<Hora>) horas);
                    spinner_hora.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();

                }else{
                    Toast.makeText(AgendamentoActivity.this,"OPS! Não conseguimos carregar as horas!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onFailure(Call<List<Hora>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AgendamentoActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void showDialog(String title, String descricao, final int idclinica){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(descricao);

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                getDatas(medicoSelecionado.getMedico_id(),idclinica);
            }
        }).setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        dialog = builder.create();
        dialog.show();
    }




    private void solicitarAgendamento(int id_medico, int id_cliente, int id_especialidade, int id_data, int id_hora, int id_clinica){



        Log.d("id_medico",String.valueOf(id_medico));
        Log.d("id_cliente",String.valueOf(id_cliente));
        Log.d("id_especialidade",String.valueOf(id_especialidade));
        Log.d("id_data",String.valueOf(id_data));
        Log.d("id_hora",String.valueOf(id_hora));
        Log.d("id_clinica",String.valueOf(id_clinica));




        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("id_medico",id_medico);
        jsonParams.put("id_cliente",id_cliente);
        jsonParams.put("id_especialidade",id_especialidade);
        jsonParams.put("id_data",id_data);
        jsonParams.put("id_hora",id_hora);
        jsonParams.put("id_clinica",id_clinica);



        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(new JSONObject(jsonParams)).toString());


        Call<ResponseBody> call  = agendamentoService.solicitarAgendamento("Bearer "+ Session.getToken(),body);


        String jsonString = new JSONObject(jsonParams).toString();
        Log.d("Body",jsonString);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.isSuccessful()){
                    showSucessMsg("Agendamento solicitado!","Em breve entraremos em contato com você.");
                    progressDialog.dismiss();
                }else{
                    Toast.makeText(AgendamentoActivity.this,"OPS! Ocorreu um erro ao tentar agendar.",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(AgendamentoActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    public void showSucessMsg(String titulo,String msg){
        succesDialog.setContentView(R.layout.msg_success_layout);
        close = (ImageView)succesDialog.findViewById(R.id.iv_close);
        btn_ok = (Button)succesDialog.findViewById(R.id.btn_ok_msg);
        tv_titulo_msg = (TextView)succesDialog.findViewById(R.id.tv_titulo_msg);
        tv_descricao_msg = (TextView)succesDialog.findViewById(R.id.tv_descricao_msg);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                succesDialog.dismiss();
                Intent i = new Intent(AgendamentoActivity.this, MenuActivity.class);
                startActivity(i);
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                succesDialog.dismiss();
                Intent i = new Intent(AgendamentoActivity.this, MenuActivity.class);
                startActivity(i);
            }
        });


        tv_titulo_msg.setText(titulo);
        tv_descricao_msg.setText(msg);



        succesDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        succesDialog.show();


    }



}
