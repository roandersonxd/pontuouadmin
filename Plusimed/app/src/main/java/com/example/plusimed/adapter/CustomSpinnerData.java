package com.example.plusimed.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.plusimed.R;
import com.example.plusimed.model.Data;

import java.util.ArrayList;


public class CustomSpinnerData extends ArrayAdapter<Data>{

    public CustomSpinnerData(@NonNull Context context, ArrayList<Data>datas) {
        super(context, 0,datas);
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,@NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_data,parent,false);
        }
        Data data = getItem(position);
        ImageView ivdata = convertView.findViewById(R.id.ivdata);
        TextView tvdata  = convertView.findViewById(R.id.tv_data);
        ivdata.setImageResource(R.drawable.ic_date_range_black_24dp);
        tvdata.setText(data.getData_agendamento());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_layout_data,parent,false);
        }
        Data data = getItem(position);
        ImageView ivdata = convertView.findViewById(R.id.ivdata);
        TextView tvdata  = convertView.findViewById(R.id.tv_data);
        tvdata.setText(data.getData_agendamento());
        ivdata.setImageResource(R.drawable.ic_date_range_black_24dp);
        return convertView;
    }
}
