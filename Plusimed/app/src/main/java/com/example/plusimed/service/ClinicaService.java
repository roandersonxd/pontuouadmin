package com.example.plusimed.service;

import com.example.plusimed.model.Clinica;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ClinicaService {


    @GET("clinica/{id_medico}")
    Call<List<Clinica>> getClinicas(@Header("Authorization") String token, @Path("id_medico") int id_medico);

    @GET("clinica")
    Call<List<Clinica>> getClinicasAll(@Header("Authorization") String token);
}
