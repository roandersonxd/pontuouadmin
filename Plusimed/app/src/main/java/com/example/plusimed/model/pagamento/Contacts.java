package com.example.plusimed.model.pagamento;

public class Contacts {

    private String type_contact;
    private String number_contact;


    public Contacts(){

    }

    public Contacts(String type_contact, String number_contact) {
        this.type_contact = type_contact;
        this.number_contact = number_contact;
    }

    public String getType_contact() {
        return type_contact;
    }

    public void setType_contact(String type_contact) {
        this.type_contact = type_contact;
    }

    public String getNumber_contact() {
        return number_contact;
    }

    public void setNumber_contact(String number_contact) {
        this.number_contact = number_contact;
    }
}
