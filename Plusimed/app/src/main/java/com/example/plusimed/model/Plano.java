package com.example.plusimed.model;

import java.io.Serializable;

public class Plano implements Serializable {

    private int id;
    private String nmplano;
    private double vlplano;




    public Plano(){

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNmplano() {
        return nmplano;
    }

    public void setNmplano(String nmplano) {
        this.nmplano = nmplano;
    }

    public double getVlplano() {
        return vlplano;
    }

    public void setVlplano(double vlplano) {
        this.vlplano = vlplano;
    }
}
