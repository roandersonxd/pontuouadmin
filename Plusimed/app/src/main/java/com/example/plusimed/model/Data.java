package com.example.plusimed.model;

public class Data {

    private int id;
    private String data_agendamento;

    public Data() {
    }

    public Data(int id, String data_agendamento) {
        this.id = id;
        this.data_agendamento = data_agendamento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData_agendamento() {
        return data_agendamento;
    }

    public void setData_agendamento(String data_agendamento) {
        this.data_agendamento = data_agendamento;
    }
}
