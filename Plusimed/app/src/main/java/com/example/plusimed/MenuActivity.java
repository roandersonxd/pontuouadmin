package com.example.plusimed;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

;

import com.example.plusimed.model.Cliente;
import com.example.plusimed.model.User;
import com.example.plusimed.service.ClienteService;
import com.example.plusimed.util.Paths;
import com.example.plusimed.util.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private CircleImageView imgUser;
    private AlertDialog dialogLogOut;
    private Context context = this;
    private Session session;
    private ClienteService clienteService;
    private int idUser;
    private Cliente cliente;
    private ProgressDialog progressDialog;
    private TextView tv_nome;
    private TextView tv_email;
    private CardView card_agendamento;
    private CardView card_perfil;
    private CardView card_carterinha;
    private  CardView card_planos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("PlusiMed");

        session = new Session(this);

        loadElements();





        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        configRetrofit();
        getUser();
        showProgressDialog(context, "", "", true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_config) {
            Intent i = new Intent(this, ConfiguracoesActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_perfil) {
            if(cliente!=null){
                Intent i = new Intent(this, PerfilActivity.class);
                i.putExtra("Cliente", cliente);
                startActivity(i);
            }else{

            }


        }else if(id==R.id.nav_sair){
            showDialogLogOut();
        }
        else if(id==R.id.nav_clinicas_proximas){
            Intent i = new Intent(this, MapsActivity.class);
            startActivity(i);
        }
        else if(id==R.id.nav_historico){
            Intent i = new Intent(this, HistoricoActivity.class);
            startActivity(i);
        }
        else if(id==R.id.nav_termo){
            Intent i = new Intent(this, TermosActivity.class);
            startActivity(i);
        }

        else if(id==R.id.nav_resultados){
            Intent i = new Intent(this, ResultadosActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void showDialogLogOut(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sair da Aplicação");
        builder.setMessage("Deseja Realmente Sair da Aplicação?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                session.setLogin(false);
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        }).setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        dialogLogOut = builder.create();
        dialogLogOut.show();
    }


    private void getUser(){


        Call<User> call  = clienteService.getUser("Bearer "+ Session.getToken());

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = null;
                if(response.isSuccessful()){
                    user = response.body();
                    Log.d("ID USER:", user.toString());
                    session.setID(user.getId());
                    getCliente(user.getEmail());


                }else{
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(MenuActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }


    private void getCliente(String emailUser){


        Call<Cliente> call  = clienteService.getCliente("Bearer "+ Session.getToken(),emailUser);

        call.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if(response.isSuccessful()){
                        cliente= response.body();
                        Log.d("Cliente", cliente.toString());
                        loadData(cliente);
                        progressDialog.dismiss();

                }else{
                    Toast.makeText(MenuActivity.this,"OPS! Não conseguimos carregar seus dados!",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(MenuActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void configRetrofit(){


        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(15, TimeUnit.SECONDS);
        client.writeTimeout(15, TimeUnit.SECONDS);




        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Paths.URL_BASE_API)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit  = builder.build();
        clienteService = retrofit.create(ClienteService.class);

    }

    public void loadElements(){
        imgUser = (CircleImageView) findViewById(R.id.iv_user);
        tv_nome = (TextView)findViewById(R.id.tv_nome);
        tv_email = (TextView)findViewById(R.id.tv_email);
        card_agendamento = (CardView)findViewById(R.id.card_agendamento);
        card_perfil = (CardView)findViewById(R.id.card_perfil);
        card_carterinha = (CardView)findViewById(R.id.card_carterinha);
        card_planos = (CardView)findViewById(R.id.card_planos);

        card_planos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, PlanosActivity.class);
                startActivity(i);
            }
        });

        card_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuActivity.this, PerfilActivity.class);
                i.putExtra("Cliente", cliente);
                startActivity(i);
            }
        });

        card_agendamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cliente.getSituacao().equals("Ativo")){
                    Intent i = new Intent(MenuActivity.this, AgendamentoActivity.class);
                    i.putExtra("Cliente", cliente);
                    startActivity(i);
                }

            }
        });
        card_carterinha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cliente!=null){
                    Intent i = new Intent(MenuActivity.this, CarterinhaActivity.class);
                    i.putExtra("Cliente", cliente);
                    startActivity(i);
                }else{

                }
            }
        });
    }
    public void loadData(Cliente cliente){
        tv_nome.setText(cliente.getNmCliente());
        tv_email.setText(cliente.getEmail());
        Picasso.get().load(Paths.URL_IMAGE+cliente.getFoto()).error(R.drawable.avatar)
               .into(imgUser);
    }

    private void showProgressDialog(Context context, String tile, String msg, boolean b){
        progressDialog = ProgressDialog.show(context,"Carregando...", "Por Favor, aguarde enquanto carregamos seus dados.");
        progressDialog.setCancelable(false);

    }

}
