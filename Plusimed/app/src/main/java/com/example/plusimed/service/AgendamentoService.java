package com.example.plusimed.service;


import com.example.plusimed.model.Data;
import com.example.plusimed.model.Hora;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AgendamentoService {


    @GET("agendamento/datas/{id_medico}/{id_clinica}")
    Call<List<Data>> getDatas(@Header("Authorization") String token, @Path("id_medico") int id_medico,@Path("id_clinica") int id_clinica);

    @GET("agendamento/horas/{id_data}")
    Call<List<Hora>> getHoras(@Header("Authorization") String token, @Path("id_data") int id_data);

    @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("agendamento")
    Call<ResponseBody> solicitarAgendamento(@Header("Authorization")String token,@Body RequestBody body);


}
