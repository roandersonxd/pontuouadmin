package com.example.plusimed.model;

import java.io.Serializable;

public class Cliente implements Serializable {
private float id;
private String nmCliente;
private String nmEndereco;
private String dscomplemento;
private String nmbairro;
private String nmcidade;
private String cduf;
private String cdcep;
private String nrcpf;
private String nrrg;
private String email;
private String nrfone;
private String dtnascimento;
private String situacao;
private float id_plano;
private float id_formapagamento;
private float id_empresa;
private String created_at;
private String updated_at;
private String foto;
private String user_id = null;
private String formapagamento;
private String empresa;
private String plano;



public float getId() {
        return id;
        }

public String getNmCliente() {
        return nmCliente;
        }

public String getNmEndereco() {
        return nmEndereco;
        }

public String getDscomplemento() {
        return dscomplemento;
        }

public String getNmbairro() {
        return nmbairro;
        }

public String getNmcidade() {
        return nmcidade;
        }

public String getCduf() {
        return cduf;
        }

public String getCdcep() {
        return cdcep;
        }

public String getNrcpf() {
        return nrcpf;
        }

public String getNrrg() {
        return nrrg;
        }

public String getEmail() {
        return email;
        }

public String getNrfone() {
        return nrfone;
        }

public String getDtnascimento() {
        return dtnascimento;
        }

public String getSituacao() {
        return situacao;
        }

public float getId_plano() {
        return id_plano;
        }

public float getId_formapagamento() {
        return id_formapagamento;
        }

public float getId_empresa() {
        return id_empresa;
        }

public String getCreated_at() {
        return created_at;
        }

public String getUpdated_at() {
        return updated_at;
        }

public String getFoto() {
        return foto;
        }

public String getUser_id() {
        return user_id;
        }

public String getFormapagamento() {
        return formapagamento;
        }

public String getEmpresa() {
        return empresa;
        }

public String getPlano() {
        return plano;
        }

// Setter Methods

public void setId(float id) {
        this.id = id;
        }

public void setNmCliente(String nmCliente) {
        this.nmCliente = nmCliente;
        }

public void setNmEndereco(String nmEndereco) {
        this.nmEndereco = nmEndereco;
        }

public void setDscomplemento(String dscomplemento) {
        this.dscomplemento = dscomplemento;
        }

public void setNmbairro(String nmbairro) {
        this.nmbairro = nmbairro;
        }

public void setNmcidade(String nmcidade) {
        this.nmcidade = nmcidade;
        }

public void setCduf(String cduf) {
        this.cduf = cduf;
        }

public void setCdcep(String cdcep) {
        this.cdcep = cdcep;
        }

public void setNrcpf(String nrcpf) {
        this.nrcpf = nrcpf;
        }

public void setNrrg(String nrrg) {
        this.nrrg = nrrg;
        }

public void setEmail(String email) {
        this.email = email;
        }

public void setNrfone(String nrfone) {
        this.nrfone = nrfone;
        }

public void setDtnascimento(String dtnascimento) {
        this.dtnascimento = dtnascimento;
        }

public void setSituacao(String situacao) {
        this.situacao = situacao;
        }

public void setId_plano(float id_plano) {
        this.id_plano = id_plano;
        }

public void setId_formapagamento(float id_formapagamento) {
        this.id_formapagamento = id_formapagamento;
        }

public void setId_empresa(float id_empresa) {
        this.id_empresa = id_empresa;
        }

public void setCreated_at(String created_at) {
        this.created_at = created_at;
        }

public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
        }

public void setFoto(String foto) {
        this.foto = foto;
        }

public void setUser_id(String user_id) {
        this.user_id = user_id;
        }

public void setFormapagamento(String formapagamento) {
        this.formapagamento = formapagamento;
        }

public void setEmpresa(String empresa) {
        this.empresa = empresa;
        }

public void setPlano(String plano) {
        this.plano = plano;
        }
    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nmCliente='" + nmCliente + '\'' +
                ", nmEndereco='" + nmEndereco + '\'' +
                ", dscomplemento='" + dscomplemento + '\'' +
                ", nmbairro='" + nmbairro + '\'' +
                ", nmcidade='" + nmcidade + '\'' +
                ", cduf='" + cduf + '\'' +
                ", cdcep='" + cdcep + '\'' +
                ", nrcpf='" + nrcpf + '\'' +
                ", nrrg='" + nrrg + '\'' +
                ", email='" + email + '\'' +
                ", nrfone='" + nrfone + '\'' +
                ", dtnascimento='" + dtnascimento + '\'' +
                ", situacao='" + situacao + '\'' +
                ", id_plano=" + id_plano +
                ", id_formapagamento=" + id_formapagamento +
                ", id_empresa=" + id_empresa +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", foto='" + foto + '\'' +
                ", user_id='" + user_id + '\'' +
                ", formapagamento='" + formapagamento + '\'' +
                ", empresa='" + empresa + '\'' +
                ", plano='" + plano + '\'' +
                '}';
    }
}