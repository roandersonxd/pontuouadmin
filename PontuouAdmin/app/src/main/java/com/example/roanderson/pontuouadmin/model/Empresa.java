package com.example.roanderson.pontuouadmin.model;

import com.google.gson.annotations.SerializedName;

public class Empresa {

    @SerializedName("enterpriseId")
    private int enterpriseId;

    @SerializedName("enterpriseName")
    private String enterpriseName;


    public Empresa(int enterpriseId, String enterpriseName) {
        this.enterpriseId = enterpriseId;
        this.enterpriseName = enterpriseName;
    }

    public int getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(int enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }
}
