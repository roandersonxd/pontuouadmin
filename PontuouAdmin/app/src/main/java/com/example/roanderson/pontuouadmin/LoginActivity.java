package com.example.roanderson.pontuouadmin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.roanderson.pontuouadmin.model.Login;
import com.example.roanderson.pontuouadmin.service.LoginService;
import com.example.roanderson.pontuouadmin.util.Paths;
import com.example.roanderson.pontuouadmin.util.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    Button btn_entrar;
    EditText edt_senha;
    EditText edt_email;
    private Session session;
    private LoginService loginService;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadElements();
        session = new Session(this);


        if(session.getLoggedIn()){
            startActivity(new Intent(LoginActivity.this, EmpresasActivity.class));
            finish();
        }

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Paths.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit  = builder.build();
        loginService = retrofit.create(LoginService.class);

        btn_entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(edt_email.getText().toString(),edt_senha.getText().toString());
            }
        });



    }


    public void loadElements(){
        btn_entrar = (Button)findViewById(R.id.btn_entrar);
        edt_email  = (EditText)findViewById(R.id.edt_email);
        edt_senha  = (EditText)findViewById(R.id.edt_senha);
    }

    private void login(String email,String senha){
        Login login  = new Login(email,senha);
        Call<ResponseBody> call  = loginService.login(login);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // response.headers().get("Authorization")
                String result = null;
                if(response.isSuccessful()){
                    try {
                        result= response.body().string();
                        JSONObject jsonObj = new JSONObject(result);
                        token = jsonObj.getString("token");
                        session.setLogin(true);
                        session.setToken(token);
                        startActivity(new Intent(LoginActivity.this, EmpresasActivity.class));
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(LoginActivity.this,"E-mail ou senha incorreto(s).",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this,"OPS! erro de comunnicação com o servidor",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
