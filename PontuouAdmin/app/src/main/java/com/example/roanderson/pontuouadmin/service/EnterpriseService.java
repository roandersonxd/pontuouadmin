package com.example.roanderson.pontuouadmin.service;


import com.example.roanderson.pontuouadmin.model.Empresa;
import com.example.roanderson.pontuouadmin.util.Paths;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface EnterpriseService {

    @Headers({
            "Cache-Control: max-age=3600",
            "User-Agent: Android",
            "Content-Type: application/json"
    })

    @GET("enterprise")
    Call<List<Empresa>>getEmpresas(@Header("Authorization") String token);






    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Paths.URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


}
