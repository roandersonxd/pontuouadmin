package com.example.roanderson.pontuouadmin.service;

import com.example.roanderson.pontuouadmin.model.Login;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface LoginService {

    @POST("login")
    Call<ResponseBody>login(@Body Login login);
}
