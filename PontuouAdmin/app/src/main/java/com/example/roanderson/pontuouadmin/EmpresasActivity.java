package com.example.roanderson.pontuouadmin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.roanderson.pontuouadmin.adapter.EmpresaAdapter;
import com.example.roanderson.pontuouadmin.model.Empresa;
import com.example.roanderson.pontuouadmin.service.EnterpriseService;
import com.example.roanderson.pontuouadmin.util.Session;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmpresasActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private Session session;
    private EnterpriseService enterpriseService = EnterpriseService.retrofit.create(EnterpriseService.class);
    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new Session(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void loadElements(){
        final ListView lista = (ListView) findViewById(R.id.lvEmpresas);
        enterpriseService = EnterpriseService.retrofit.create(EnterpriseService.class);
    }

    @Override
    protected void onStart() {
        super.onStart();


        dialog = new ProgressDialog(EmpresasActivity.this);
        dialog.setMessage("Carregando...");
        dialog.setCancelable(false);
        dialog.show();



        final Call<List<Empresa>> call = enterpriseService.getEmpresas(getSession().getToken());
        call.enqueue(new Callback<List<Empresa>>() {
            @Override
            public void onResponse(Call<List<Empresa>> call, Response<List<Empresa>> response) {
                if (dialog.isShowing())
                    dialog.dismiss();


                //aqui recebe a lista
                final List<Empresa> listaEmpresas = response.body();

                if (listaEmpresas != null) {
                    EmpresaAdapter adapter = new EmpresaAdapter(getBaseContext(), listaEmpresas);
                    lista.setAdapter(adapter);
                    lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("NOME EMPRESA",listaEmpresas.get(i).getEnterpriseName());


                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<List<Empresa>> call, Throwable t) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getBaseContext(), "Problema de acesso", Toast.LENGTH_LONG).show();
            }
        });

    }

    public Session getSession() {
        return session;
    }
}
