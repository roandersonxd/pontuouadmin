package com.example.roanderson.pontuouadmin.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {
    private static SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    public Session(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public boolean setLogin(boolean status) {
        spEditor = sp.edit();
        spEditor.putBoolean("is_logged_in", status);
        spEditor.commit();
        return true;
    }


    public void setToken(String token){
        spEditor = sp.edit();
        spEditor.putString("token", token);
        spEditor.commit();
    }

    public void setID(String id){
        spEditor = sp.edit();
        spEditor.putString("idEmpresa", id);
        spEditor.commit();
    }

    public void setEmail(String emailEmpresa){
        spEditor = sp.edit();
        spEditor.putString("emailEmpresa", emailEmpresa);
        spEditor.commit();
    }

    public void setIDClient(int idClient){
        spEditor = sp.edit();
        spEditor.putInt("IDCliente", idClient);
        spEditor.commit();
    }


    public static String getToken(){
        return sp.getString("token", "");
    }
    public static String getID(){
        return sp.getString("idEmpresa", "");
    }

    public static String getEmailEmpresa(){
        return sp.getString("emailEmpresa", "");
    }

    public static int getIDClient(){
        return sp.getInt("IDCliente", 0);
    }


    public boolean getLoggedIn() {
        return sp.getBoolean("is_logged_in", false);
    }
}
