package com.example.roanderson.pontuouadmin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.roanderson.pontuouadmin.R;
import com.example.roanderson.pontuouadmin.model.Empresa;

import java.util.List;

public class EmpresaAdapter extends ArrayAdapter<Empresa> {
    private final Context context;
    private final List<Empresa> elementos;

    public EmpresaAdapter(Context context, List<Empresa> elementos) {
        super(context, R.layout.item_list_empresas, elementos);
        this.context = context;
        this.elementos = elementos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_list_empresas, parent, false);

        TextView titulo = (TextView) rowView.findViewById(R.id.txtNome);
        TextView codigo = (TextView) rowView.findViewById(R.id.txtCodigo);

        titulo.setText(elementos.get(position).getEnterpriseName());
        codigo.setText(elementos.get(position).getEnterpriseId());

        return rowView;
    }
}